import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Activities } from './pages/Activities'
import { DetailActivity } from './pages/DetailActivity'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 data-cy="header-title" className='header-title'>TO DO LIST APP</h1>
      </header>
      <main className='main-wrapper'>
        <Router>
          <Routes>
              <Route path='/' element={<Activities />}/>
              <Route path='/detail/:id' element={<DetailActivity/>}/>
          </Routes>  
        </Router> 
      </main>
    </div>
  );
}

export default App;
