import React, {useEffect, useState, useRef} from "react"
import back from '../images/back-icon.svg'
import { useParams, useNavigate } from "react-router-dom"
import axios from "axios"
import emptyDetail from '../images/todo-empty-state.png'
import edit from '../images/edit-icon.svg'
import latest from '../images/latest-icon.svg'
import least from '../images/least-icon.svg'
import asc from '../images/ascending-icon.svg'
import desc from '../images/descending-icon.svg'
import unfinished from '../images/unfinished-icon.svg'
import { TodoList } from "../components/TodoList"
import { ModalTodos } from "../components/ModalTodos"
import {  TextField } from '@mui/material';
import SwapVertIcon from '@mui/icons-material/SwapVert';
import { Card } from '@mui/material';
import check from '../images/check-icon.svg'


const baseURL = 'https://todo.api.devcode.gethired.id'

const sortList = [
    { text: 'Terbaru', value: 'latest', icon: latest},
    { text: 'Terlama', value: 'least', icon: least},
    { text: 'A-Z', value: 'asc', icon: asc},
    { text: 'Z-A', value: 'desc', icon: desc},
    { text: 'Belum Selesai', value: 'unfinished', icon: unfinished},
]

export const DetailActivity = () => {
    const { id } = useParams()
    const navigate = useNavigate()
    const [title, setTitle] = useState('')
    const [newTitle, setNewTitle] = useState('')
    const [isOutside, setIsOutside] = useState(false)
    const [detail, setDetail] = useState({})
    const [open, setOpen] = useState(false)
    const [isEdit, setIsEdit] = useState(false)
    const [isSort, setIsSort] = useState(false)
    const [isError, setIsError] = useState(false)
    const [sort, setSort] = useState('latest')

    const getDetail = () => {
        axios.get(`${baseURL}/activity-groups/${id}`).then((res) => {
            if(res.data) {
                setDetail(res.data.todo_items)
                setTitle(res.data.title)
            }
        }).catch(err => {
            setIsError(true)
        })
    }
    useEffect(getDetail, [id])

    const handleCheck = (detail) => {
        axios.patch(`${baseURL}/todo-items/${detail.id}`, {
            is_active: !detail.is_active ? 1 : 0 
        }).then((res) => {
            getDetail()
        })
    }

    const toggleEdit = () => {
        setIsEdit(!isEdit)
        if(isEdit) {
            updateData()
        }
    }

    const updateData = async () => {
        await axios.patch(`${baseURL}/activity-groups/${id}`, {
            title: newTitle
        })
        getDetail()
    }

    const useOutsideAlerter = (callback) => {
        const ref = useRef()
        useEffect(() => {
          function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setIsEdit(false)
                setIsOutside(true)
            }
          }
    
          document.addEventListener("click", handleClickOutside);
          return () => {
            document.removeEventListener("click", handleClickOutside);
          };
        }, [ref]);
        return ref
      }

    const wrapperRef = useOutsideAlerter(updateData);

    const handleUpdate = (e) => {
        if(e.keyCode === 13) {
            updateData()
            toggleEdit()
        }
    }

    const toggleSort = () => {
        setIsSort(!isSort)
    }

    const handleClick = (event) => {
        event.stopPropagation();
      };

    useEffect(() => {
        if(isOutside) {
            updateData()
        }
    }, [isOutside])

    const handleSort = () => {
        if(sort === 'least') {
            let filter = detail.reverse()
            setDetail(filter)
        } 
        else if(sort === 'asc') {
            let filter = detail.sort((a,b) => {
                let aTitle = a.title, bTitle = b.title

                if(aTitle < bTitle) return -1
                if(aTitle > bTitle) return 1
                return 0
            })
            setDetail(filter)
        } else if(sort === 'desc') {
            let filter = detail.sort((a,b) => {
                let aTitle = a.title, bTitle = b.title

                if(aTitle > bTitle) return -1
                if(aTitle < bTitle) return 1
                return 0
            })
            setDetail(filter)
        }
        else if(sort === 'unfinished') {
            let filter = detail.sort((a, b) => {
                let sortA = a.is_active, sortB = b.is_active
                if(sortA > sortB) return -1
                if(sortA < sortB) return 1
                return 0
            })
            setDetail(filter)  
        }
    }

    return (
        <div>
            {
                !isError && 
                <div>
                    <div className="header-activity">
                        <div style={{display: 'flex'}}>
                            <button onClick={() => navigate('/')}>
                                <img src={back} alt='back-icon'/>
                            </button>
                            <div onClick={handleClick} style={{display:'flex'}}>
                                {
                                    isEdit ? <TextField ref={wrapperRef} data-cy='edit-activity' variant="standard" defaultValue={title || newTitle} onChange={(e) => setNewTitle(e.target.value)} onKeyDown={handleUpdate}  className='edit-activity' /> :  <h1 data-cy='todo-title' className="header-activity-title" onClick={toggleEdit}>{ title }</h1>
                                }
                                                
                                <button data-cy='todo-title-edit-button' onClick={toggleEdit}>
                                    <img src={edit} alt='edit-icon' width={'30px'} />
                                </button>
                            </div>
                        </div>
                        <div style={{display: 'flex', position:'relative'}}>
                            <button data-cy='todo-sort-button' className="sort-btn" onClick={toggleSort}>
                                <SwapVertIcon />
                            </button>
                            {
                                isSort ? 
                                <Card data-cy='sort-parent' className="sort-list">
                                    {
                                        sortList.map(arr => (
                                            <div data-cy='sort-selection' key={arr.value} className="sort-item" onClick={() => {setSort(arr.value); toggleSort()}}>
                                                <div>
                                                    <img src={arr.icon} alt={arr.value+'-icon'} width={'20px'} style={{marginRight:'16px'}} />
                                                    <span>{ arr.text }</span>
                                                </div>
                                                { arr.value === sort ?  <img src={check} alt='check-icon' /> : <></>}
                                               
                                            </div>
                                        ))
                                    }
                                </Card>
                                : <></>
                            }
                            
                            <button data-cy='todo-add-button' className="add-btn" onClick={() => setOpen(true)}>
                            + Tambah
                            </button>
                        </div>
                    </div>
                    <ModalTodos open={open} setOpen={setOpen} id={id} getDetail={getDetail} />
                    {
                        detail?.length ? <TodoList detail={detail} handleCheck={handleCheck} getDetail={getDetail} sort={sort} /> : <img data-cy='todo-empty-state' src={emptyDetail} alt="empty-detail-state"></img>
                    }
                </div>
            }
        </div>
    )
}