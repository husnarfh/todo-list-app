import React, {useState, useEffect} from "react"
import { EmptyStateDashboard } from "../components/EmptyStateDashboard"
import { ActivityList } from "../components/ActivityList"
import axios from "axios"
// import AddIcon from '@mui/icons-material/Add';

const baseURL = 'https://todo.api.devcode.gethired.id'

export const Activities = () => {
    const [activities, setActivities] = useState([])

    const getActivities = () => {
        axios.get(`${baseURL}/activity-groups?email=husnanrarifah@gmail.com`).then((res) => {
            setActivities(res.data.data)
        }).catch(err => {
            console.log(err)
        })
    }

    useEffect(() => {
        getActivities()
    }, [])

    const addActivity = e => {
        axios.post(`${baseURL}/activity-groups`, {
            title: 'New Activity',
            email: 'husnanrarifah@gmail.com'
        }).then((res) => {
            getActivities()
        })
    }

    return (
        <div>
            <div className="header-activity">
                <h1 data-cy='activity-title' className="header-activity-title">Activity</h1>
                <button data-cy='activity-add-button' className="add-btn" onClick={addActivity}>
                   + Tambah
                </button>
            </div>
            { activities?.length > 0 ? <ActivityList activities={activities} getActivities={getActivities} /> : <EmptyStateDashboard data-cy='activity-empty-state'/> }
           
        </div>
    )
}