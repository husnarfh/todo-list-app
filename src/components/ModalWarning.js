import React, {useState,  useRef, useEffect} from "react";
import { Modal, Card, CardActions, CardContent } from '@mui/material';
import warning from '../images/warning-icon.svg'
import axios from "axios";
import info from '../images/info-icon.svg'

const baseURL = 'https://todo.api.devcode.gethired.id'

export const ModalWarning = ({open, setOpen, item, type, getActivities, getDetail}) => {  
    const [isSnackbar, setSnackbar] = useState(false)
    const handleClose = () => {
        setOpen(false)
        setSnackbar(false)
    };

    const handleDelete = async () => {
        if(type === 'activity') {
            await axios.delete(`${baseURL}/activity-groups/${item.id}`).then((res) => {
                if(res) setSnackbar(true); getActivities()
            })
        } else {
            await axios.delete(`${baseURL}/todo-items/${item.id}`).then((res) => {
                if(res) setSnackbar(true); getDetail()
            })
        }   
    }

  const useOutsideAlerter = (ref) => {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
            handleClose()
        }
      }

      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

    return (
            <Modal
                open={open}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                style={{display:'flex', alignContent:'center', flexWrap:'wrap'}}
                >
                   {
                    isSnackbar ? 
                    <Card data-cy='modal-information' style={{margin: '0 auto'}} ref={wrapperRef}>
                        <CardContent style={{display:'flex', paddingBottom:'14px'}}>
                            <img src={info} alt='info-icon' style={{marginRight: '14px'}} />
                            {type === 'activity' ? 'Activity' : 'List item'} berhasil dihapus
                        </CardContent>
                    </Card>
                   :
                    <Card data-cy='modal-delete' ref={wrapperRef} className='warning-modal'>
                        <div style={{display:'flex'}}>
                            <img data-cy='modal-delete-icon' src={warning} alt='warning-icon' width={84} style={{margin: '0 auto', paddingTop: '32px'}}/>
                        </div>
                        <CardContent sx={{ px: 6, textAlign:'center'}}>
                            Apakah anda yakin menghapus {type === 'activity' ? 'activity' : 'List Item'} <br/> <b>“{ item.title }”</b>?
                        </CardContent>
                        <CardActions className="dialog-actions" sx={{px: 6, py: 4}}>
                            <div style={{display:'flex', gap: '20px'}}>
                                <button data-cy='modal-delete-cancel-button' className="cancel-btn" onClick={handleClose}>Batal</button>
                                <button data-cy='modal-delete-confirm-button' className="delete-btn" onClick={handleDelete}>Hapus</button>
                            </div>
                        </CardActions>
                    </Card>
                    }
            </Modal>
            
    )
}