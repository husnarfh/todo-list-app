import React, {useEffect, useState} from "react";
import { Card, Checkbox } from '@mui/material';
import trash from '../images/trash-icon.svg'
import edit from '../images/edit-icon.svg'
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { ModalWarning } from "./ModalWarning";
import { ModalTodos } from "./ModalTodos";

const priorityDot = {
    'very-high': '#ED4C5C',
    'high': '#F8A541',
    'normal': '#00A790',
    'low': '#428BC1',
    'very-low':'#8942C1'
}

export const TodoList = ({detail, handleCheck, getDetail, sort}) => {
    const [selected, setSelected] = useState({})
    const [filter, setFilter] = useState([])
    const [selectedItem, setSelectedItem] = useState({})
    const [open, setOpen] = useState(false);
    const [openTodos, setOpenTodos] = useState(false);
    
    useEffect(() => {
        getDetail()
    }, [open])

    const handleSort = () => {
        if(sort === 'least') {
            let filter = detail.reverse()
            setFilter(filter)
        } 
        else if(sort === 'asc') {
            let filter = detail.sort((a,b) => {
                let aTitle = a.title, bTitle = b.title

                if(aTitle < bTitle) return -1
                if(aTitle > bTitle) return 1
                return 0
            })
            setFilter(filter)
        } else if(sort === 'desc') {
            let filter = detail.sort((a,b) => {
                let aTitle = a.title, bTitle = b.title

                if(aTitle > bTitle) return -1
                if(aTitle < bTitle) return 1
                return 0
            })
            setFilter(filter)
        }
        else if(sort === 'unfinished') {
            let filter = detail.sort((a, b) => {
                let sortA = a.is_active, sortB = b.is_active
                if(sortA > sortB) return -1
                if(sortA < sortB) return 1
                return 0
            })
            setFilter(filter)  
        }
    }

    useEffect(() => {
       if(sort === 'latest') {
        setFilter(detail)
       } else {
        handleSort()
       }
    }, [sort])

    return (
        <div>
            {
                detail.map((arr, idx) => (
                    <Card data-cy={`todo-item-`+idx} className="card-todo" key={arr.id}>
                        <div className="card-todo-list">
                            <Checkbox data-cy='todo-item-checkbox' checked={arr.is_active === 0} onClick={() => handleCheck(arr)} />
                            <FiberManualRecordIcon style={{ color: priorityDot[arr.priority], height: '16px', padding: '0 12px' }} />
                           <p data-cy='todo-item-title' className={arr.is_active === 0 ? 'card-todo-done' : ''} style={{fontSize: '18px'}}>{ arr.title }</p>
                           <button style={{marginLeft:'16px'}} onClick={() => {setSelectedItem(arr); setOpenTodos(true)}}>
                            <img src={edit} alt='edit-icon'/>
                           </button>
                        </div>
                        <button data-cy='todo-item-delete-button' onClick={() => {setSelected(arr); setOpen(true)}} >
                            <img src={trash} alt='delete-icon' height={24}/>
                        </button>
                    </Card>
                ))
            }
            <ModalWarning open={open} setOpen={setOpen} item={selected} type='list-item' getDetail={getDetail}/> 
            <ModalTodos open={openTodos} setOpen={setOpenTodos} getDetail={getDetail} item={selectedItem} />
        </div>
    )
}