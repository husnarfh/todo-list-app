import React from "react";
import empty from '../images/activity-empty-state.png'


export const EmptyStateDashboard = () => {
    return (
        <div>
            <img src={empty} alt="empty-state"></img>
        </div>
    )
}