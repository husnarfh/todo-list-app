import React, {useState} from 'react'
import { Card, CardActions, Grid } from '@mui/material';
import trash from '../images/trash-icon.svg'
import moment from 'moment';
import { ModalWarning } from './ModalWarning';


export const ActivityList = ({activities, getActivities}) => {
    const [selected, setSelected] = useState({})
    const [open, setOpen] = useState(false);

    return (
        <div>
            <Grid container spacing={3}>
            {
                activities.map((act, idx) => (
                <Grid key={act.id} item md={3}>
                    <Card className='card-activities'>
                        <a data-cy='activity-item' href={`/detail/${act.id}`} style={{ color:'black', textDecoration:'none'}}>
                            <h3 data-cy='activity-item-title' className='activity-title'> { act.title }</h3>
                        </a>
                        <CardActions className='activity-action'>
                            <p data-cy='activity-item-date' style={{color:'#888888'}}>{ moment(act.created_at).locale('id').format('DD MMMM YYYY') }</p> 
                            <button data-cy='activity-item-delete-button' onClick={() => {setSelected(act); setOpen(true)}}>
                                <img src={trash} alt='delete-icon' />
                            </button>
                        </CardActions>
                    </Card>
                </Grid>
                ))
            }
            </Grid>
                <ModalWarning open={open} setOpen={setOpen} item={selected} type='activity' getActivities={getActivities} /> 
        </div>
    )
}