import React, { useState,useEffect } from "react";
import { Modal, CardContent, CardActions, TextField, Card } from '@mui/material';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import CloseIcon from '@mui/icons-material/Close';
import axios from "axios";
import check from '../images/check-icon.svg'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';

const baseURL = 'https://todo.api.devcode.gethired.id'

const priorityList = [
    { name: 'Very High', value: 'very-high', color: '#ED4C5C'},
    { name: 'High', value: 'high', color: '#F8A541'},
    { name: 'Medium', value: 'normal', color: '#00A790'},
    { name: 'Low', value: 'low', color: '#428BC1'},
    { name: 'Very Low', value: 'very-low', color: '#8942C1'}
]

export const ModalTodos = ({open, setOpen, getDetail, id, item}) => {
    const [priority, setPriority] = useState('');
    const [todos, setTodos] = useState('')
    const [isToggle, setToggle] = useState(false)
    const [isDisable, setDisable] = useState(true)

    const handleClose = () => {
        setOpen(false)
        setPriority('')
        setTodos('')
    }
    const addTodos = () => {
        axios.post(`${baseURL}/todo-items`, {
            activity_group_id: id,
            title: todos,
            priority: priority.value
        }).then(res => {
            setOpen(false)
            setPriority('')
        })
    }
    const editTodos = () => {
        axios.patch(`${baseURL}/todo-items/${item.id}`, {
            title: todos,
            priority: priority.value,
            is_active: item.is_active
        }).then(res => {
            setOpen(false)
            setPriority('')
        })
    }
    useEffect(() => {
        getDetail()
    }, [open])

    useEffect(() => {
        if(item) setPriority(priorityList.find(x => x.value === item.priority))
    }, [item])

    const togglePriority = () => {
        setToggle(!isToggle)
    }

    useEffect(() => {
        if(priority && todos) {
            setDisable(false)
        } else setDisable(true)
    }, [priority, todos])

    return (    
         <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            style={{display:'flex', alignContent:'center', flexWrap:'wrap'}}
        >
            <Card data-cy='modal-add' className="add-item">
                <div style={{display: 'flex', justifyContent: 'space-between', padding:'4px 30px'}}>
                    <h4>{ item ? 'Edit' : 'Tambah'} List Item</h4>
                    <button onClick={handleClose}>
                        <CloseIcon/>
                    </button>
                </div>
                <CardContent style={{borderTop: '1px solid #E5E5E5', borderBottom: '1px solid #E5E5E5', padding: '38px 30px'}} >
                    <p className="title-form">NAMA LIST ITEM</p>
                    <TextField data-cy='modal-add-name-input' id="outlined-basic" placeholder="Tambahkan nama list item" className="list-item" variant="outlined" onChange={(e) => setTodos(e.target.value)} fullWidth defaultValue={item?.title}/>

                    <p className="title-form">PRIORITY</p>
                    <button data-cy='modal-add-priority-dropdown' className="choose-prio" onClick={togglePriority}>
                        <div style={{display:'flex', justifyContent:'space-between', width:'100%'}}>
                            
                                <div style={{display:'flex'}}>
                                    {priority && <FiberManualRecordIcon style={{color: priority?.color, paddingRight:'8px'}} />}
                                    <span>{priority ? priority.name : 'Pilih Priority'}</span>  
                                </div>
                            { isToggle ? <ExpandLessIcon/> : <ExpandMoreIcon /> }  
                        </div>
                        {
                            isToggle && <Card className="prio-list">
                            {
                                priorityList.map(arr => (
                                    <div data-cy='modal-add-priority-item' key={arr.value} className="prio-item" onClick={() => {setPriority(arr); togglePriority()}}>
                                        <div style={{ display:'flex'}}>
                                            <FiberManualRecordIcon style={{color: arr.color, paddingRight:'19px'}} />
                                            <span>{ arr.name }</span>
                                        </div>
                                        { arr.value === priority.value ?  <img src={check} alt='check-icon' /> : <></>}
                                    </div>
                                ))
                            }
                        </Card> 
                        }
                    </button>
                </CardContent>
                <CardActions sx={{display:'flex', justifyContent:'end'}}>
                    <button data-cy='modal-add-save-button' disabled={isDisable} type="submit" className={isDisable? 'disable-btn' : 'save-button'} onClick={item ? editTodos : addTodos}>Simpan</button>
                </CardActions>
            </Card>
        </Modal>
    )
}